module.exports=async function(crawl) {
	let result=[];
	let numPages=crawl.input.pages;

	if (!numPages)
		numPages=0;

	if (numPages<1)
		numPages=1;

	if (numPages>5)
		numPages=5;

	crawl.progress(10);
	const page = crawl.getPage();
	crawl.progress(20);

	let url="https://news.ycombinator.com/";
	//let url="file:///home/micke/Desktop/Hacker%20News.html";
	let pageNum=0;

	while (pageNum<numPages && url) {
		//console.log(url);
		await page.goto(url);
		await crawl.injectJquery();

		crawl.progress(20+80*pageNum/numPages);

		var pageData=await page.evaluate(function() {
			var result=[];

			$("tr.athing").each(function(index){
				result.push({
					rank: parseInt($(this).find(".rank").text()),
					title: $(this).find(".storylink").text(),
					link: $(this).find(".storylink").attr("href"),
					score: parseInt($(this).next("tr").find(".score").text()),
					author: $(this).next("tr").find(".hnuser").text(),
					time: $(this).next("tr").find(".age").text(),
					comments: parseInt($(this).next("tr").find("a:eq(3)").text()),
				});
			});

			let morelink=$("a.morelink").attr("href");

			return {
				result: result,
				morelink: morelink
			};
		});

		result=[...result,...pageData.result];
		url="https://news.ycombinator.com/"+pageData.morelink;
		pageNum++;
	}

	crawl.done(result);
};